<%--
  Created by IntelliJ IDEA.
  User: Princy
  Date: 28/01/2023
  Time: 00:47
  To change this template use File | Settings | File Templates.
--%>
<%@page import="model.Acteur"%>
<%@page import="model.Scene"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"></jsp:include>


    <center><h2 style="color: #7b70be;"><i class="ion ion-md-videocam"></i> Ajout Action </h2></center>


    <br/>
<%
    String info = "";

    if (request.getAttribute("info") != null) {
        info = request.getAttribute("info").toString();%>

<center>
    <div style="width: 400px;" class="alert alert-success" role="alert">
        <strong>Succes</strong> <%= info%>
    </div>
</center>

<%   } else if (request.getAttribute("error") != null) {
    String error = request.getAttribute("error").toString();
%>
<center>
    <div style="width: 400px;" class="alert alert-danger mb-0" role="alert">
        <strong>Error</strong> <%= error%>
    </div>
</center>
<% }%>


<br>

<%
    List<Scene> scene = (List<Scene>) request.getAttribute("scene");
    List<Acteur> acteur = (List<Acteur>) request.getAttribute("acteur");
%>


<div class="row" style="margin-left: 280px;">
    <div class="col-xl-6" style="width: 700px;">
        <div class="card">
            <div class="card-body">
                <center><h4 class="card-title">Formulaire </h4></center>
                <form class="custom-validation" action="<%= request.getContextPath()%>/insertAction" method="post" >

                    <div class="mb-3">
                        <label class="form-label">Scene</label>
                        <select class="form-control select2" name="idscene">
                            <%
                                for(Scene a : scene){
                            %>
                            <option value="<%= a.getId() %>"><%= a.getNom() %></option>
                            <%
                                }
                            %>
                        </select>
                    </div>


                    <div class="mb-3">
                        <label class="form-label">Acteur</label>
                        <select class="form-control select2" name="idacteur">
                            <%
                                for(Acteur s : acteur){
                            %>
                            <option value="<%= s.getId() %>"><%= s.getNom() %></option>
                            <%
                                }
                            %>
                        </select>
                    </div>


                    <div class="mb-3">
                        <label class="form-label">Sentiment</label>
                        <div>
                            <input data-parsley-type="alphanum" name="sentiment" type="text"
                                   class="form-control" required
                                   placeholder=""/>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Dialogue</label>
                        <div>
                            <textarea name="dialogue" required class="form-control" placeholder="Contenu" rows="5"></textarea>
                        </div>
                    </div>


                    <div class="mb-3">
                        <label class="form-label">Action</label>
                        <div>
                            <textarea name="action" required class="form-control" placeholder="Contenu" rows="4"></textarea>
                        </div>
                    </div>

                    <div class="mb-3">
                        <label>Durée</label>
                        <div>
                            <div class="input-group" >
                                <input name="duree" min="1" type="number" class="form-control" >
                            </div>
                        </div>
                    </div>
                    
                    <div class="mb-3">
                        <label>Ordre</label>
                        <div>
                            <div class="input-group" >
                                <input name="ordre" min="1" type="number" class="form-control" >
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                                Add
                            </button>
                            <button type="reset" class="btn btn-secondary waves-effect">
                                Cancel
                            </button>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>

</div>





<jsp:include page="footer.jsp"></jsp:include>