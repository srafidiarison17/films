<%--
  Created by IntelliJ IDEA.
  User: Princy
  Date: 28/01/2023
  Time: 00:47
  To change this template use File | Settings | File Templates.
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Scene"%>
<%@page import="model.Plateau"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"></jsp:include>


    <center><h2 style="color: #7b70be;"><i class="ion ion-md-aperture"></i> Ajout Scène </h2></center>


    <br/>
<% try {
    String info = "";
    //List<Plateau> li = (List<Plateau>) request.getAttribute("plateau");
    Object[] obj=( Object[])request.getAttribute("plateau");
        List<Plateau> li = new ArrayList<>();
       
                 for (int i = 0; i < obj.length; i++) {
                li.add((Plateau)obj[i]);
            }
            
       
    int idfilm = (int) session.getAttribute("idfilm");

    if (request.getAttribute("info") != null) {
        info = request.getAttribute("info").toString();%>

<center>
    <div style="width: 400px;" class="alert alert-success" role="alert">
        <strong>Succes</strong> <%= info%>
    </div>
</center>

<%   } else if (request.getAttribute("error") != null) {
    String error = request.getAttribute("error").toString();
%>
<center>
    <div style="width: 400px;" class="alert alert-danger mb-0" role="alert">
        <strong>Error</strong> <%= error%>
    </div>
</center>
<% }%>


<br>


<div class="row" style="margin-left: 280px;">
    <div class="col-xl-6" style="width: 700px;">
        <div class="card">
            <div class="card-body">
                <center><h4 class="card-title">Formulaire </h4></center>
                <form class="custom-validation" action="<%= request.getContextPath()%>/AjoutScene" method="post">
                    <div class="mb-3">
                        <label class="form-label">Plateau</label>
                        <select class="form-control select2" name="idplateau">
                            <%
                                for (Plateau p : li) {
                            %>
                            <option value="<%= p.getIdplateau()%>"><%= p.getNom()%></option>
                            <%
                                }
                            %>
                        </select>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Nom</label>
                        <div>
                            <input data-parsley-type="alphanum" name="nom" type="text"
                                   class="form-control" required
                                   placeholder="Titre"/>
                        </div>
                    </div>


                    <div class="mb-3" >
                        <label>Durée </label>
                        <div>
                            <div class="input-group" >
                                <input name="duree" type="time" class="form-control" >
                            </div>
                        </div>
                    </div>

                    <div class="mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                                Create
                            </button>
                            <button type="reset" class="btn btn-secondary waves-effect">
                                Cancel
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>


<br><!-- comment -->
<%
    //List<Scene> s = (List<Scene>) request.getAttribute("scene");
    //Scene[] s = (Scene[]) request.getAttribute("scene");
    Object[] objs=( Object[])request.getAttribute("scene");
    out.println("scene"+objs.length);
    List<Scene> s = new ArrayList<Scene>();
        for (int i = 0; i < objs.length; i++) {
                s.add((Scene)objs[i]);
            }
%>

<div class="row" style="width: 500px">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Listes des Scenes du film</h4>

                <div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12"><table id="datatable" class="table table-bordered dt-responsive nowrap dataTable no-footer dtr-inline" style="border-collapse: collapse; border-spacing: 0px; width: 100%;" role="grid" aria-describedby="datatable_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 193.6px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">ID</th>
                                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 289.6px;" aria-label="Position: activate to sort column ascending">Scene</th>
                                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 289.6px;" aria-label="Position: activate to sort column ascending">Plateau</th>
                                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 143.6px;" aria-label="Office: activate to sort column ascending">Durée</th>
                                </thead>
                                <%
                                    for (Scene sc : s) {
                                %>
                                <tbody>
                                    <tr role="row" class="odd">
                                        <td class="sorting_1 dtr-control"><%= sc.getIdplateau()%></td>
                                        <td><%= sc.getNom()%></td>
                                        <td><%= sc.getIdplateau()%></td>
                                        <td><%= sc.getDuree()%></td>
                                    </tr>
                                </tbody>
                                <%
                                    }
                                } catch (Exception e) {
           out.println("err"+e.getMessage());
e.printStackTrace();
            }%>
                            </table></div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="datatable_previous"><a href="#" aria-controls="datatable" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="datatable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="datatable" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="datatable" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="datatable" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="datatable" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item "><a href="#" aria-controls="datatable" data-dt-idx="6" tabindex="0" class="page-link">6</a></li><li class="paginate_button page-item next" id="datatable_next"><a href="#" aria-controls="datatable" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>

            </div>
        </div>
    </div>
    <!-- end col -->
</div>

<div>
    
    <form action="<%= request.getContextPath() %>/datesDispo" method="POST">
        <div class="mb-3">
            <label class="form-label">Plateau</label>
            <select name="nomPlateau">
                <c:forEach var="plat" items="${plateau}">
                    <option value="${plat.nom}">${plat.nom}</option>
                </c:forEach>
            </select>
        </div>
        <input type="submit" value="voir les dates disponibles">
    </form>
</div>




<jsp:include page="footer.jsp"></jsp:include>