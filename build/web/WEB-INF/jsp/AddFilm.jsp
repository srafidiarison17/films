<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"></jsp:include>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajout film</title>
</head>
<body>
    <h3 style="color: darkcyan;">Ajout de film</h3>

    <form action="AjoutPic" method="POST" enctype="multipart/form-data">
        <table style="width: 350px;">
            <tr>
                <td>Titre</td>
                <td><input type="text" name="titre"></td>
            </tr>
            <tr>
                <td>Dur&eacute;e</td>
                <td><input type="time" name="duree"></td>
            </tr>
            <tr>
                <td>Auteur</td>
                <td><input type="text" name="auteur"></td>
            </tr>
            <tr>
                <td>Couverture</td>
                <td><input type="file" name="photo"></td>
            </tr>
            <tr>
                <td><input type="submit" value="Add"></td>
            </tr>
        </table>
    </form>
</body>
</html>