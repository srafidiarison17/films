<%@page import="model.Scene"%>
<%@page import="java.util.List"%>
<%@page import="model.Film"%>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"></jsp:include>

<form action="<%= request.getContextPath() %>/SuggestPlan" method="get" >
<div class="card" style="width: 300px;float: left;">
    <div class="card-body">
        <div class="row">
            <form action="<%= request.getContextPath() %>/recherche" method="get" >
                <div class="mb-3">
                    <label class="form-label">Date début</label>
                        <input data-parsley-type="alphanum" name="debut" type="date"
                               class="form-control"
                               />
                     <br/>
                     <br/>
                     <label class="form-label">Date fin</label>
                        <input data-parsley-type="alphanum" name="fin" type="date"
                               class="form-control"
                               />
                     <br/>
                     <br/>
                    <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                        Suggérer planning
                    </button>
                </div>
            
        </div>
    </div>
</div>

<div class="row" style="clear: right;">
    <%
        List<Scene> ls = (List<Scene>) request.getAttribute("scene");
        for(Scene s : ls) {
    %>
    <div class="col-lg-4">
                    <div class="card">
                        
                        <div class="card-body">
                            <input type="checkbox" name="scenes" value="<%=s.getIdscene()%>">
                            <h3 class="card-title"><%= s.getNom() %> </h3>
                            <p>Durée  : <%= s.getDuree() %> </p>
                            
                            <p class="card-text">
                            <p style="color: #7b70be;font-size: 12px;">Plateau : <%= s.getIdplateau()%> </p>
                           
                            <p style="color: #7b70be;font-size: 12px;">Ordre :  </p>
                            </p>
                             <p style="color: #7b70be;font-size: 12px;">Tranche horaire : <%= s.getHorairedebut()%>- <%= s.getHorairefin()%></p>
                            </p>

                       
                            
                        </div>
                    </div>
                </div>
    <%
        }
    %>
</div>
</form>
<!--nav style="margin-left: 500px;" aria-label="Page navigation example">
    <ul class="pagination">
        <li class="page-item"><a class="page-link" href="<%= request.getContextPath() %>/paginate/1">1</a></li>
        <li class="page-item"><a class="page-link" href="<%= request.getContextPath() %>/paginate/2">2</a></li>
        <li class="page-item"><a class="page-link" href="<%= request.getContextPath() %>/paginate/3">3</a></li>
    </ul>
</nav-->
<jsp:include page="footer.jsp"></jsp:include>