<%-- 
    Document   : front
    Created on : 29 janv. 2023, 23:45:12
    Author     : Mitasoa
--%>

<%@page import="dao.HibernateDAO"%>
<%@page import="model.Photo"%>
<%@page import="java.util.List"%>
<%@page import="model.Publication"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Publication> mots = (List<Publication>) request.getAttribute("pub");
    HibernateDAO dao = (HibernateDAO)request.getAttribute("hibernate");
    
%>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Super Admin</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/img/assets/bootstrap/css/bootstrap.min.css">
</head>

<body>
    <header>
        <h1>Ny VAOVAO</h1>
    </header>
    
    <div class="card-group">
    <body>
        <h1>Super Admin</h1>
        <table border="1">
            <tr>
                <th>Visible on homepage</th>
                <th>Titre</th>
                <th>Date 1</th>
                <th>Date 2</th>
                <th>Date Publication</th>
                <th>Lieu</th>
                <th>Description</th>
                <th>Photo</th>
                <th>Etat</th>
            </tr>
               <form action="superAdmin" method="GET">
                   <input type="submit" value="Modifier la visibilite">
            <% for (Publication mot : mots) {%>
         
            <tr>
                  <%if(mot.getVisibility()==0){out.println("<td><input type='checkbox' id='id' name='visibilite' value='"+mot.getId()+"'></td>");} 
                else{out.println("<td><input type='checkbox' id='id' checked  name='visibilite' value='"+mot.getId()+"'></td>");}%>
                
                <td><%=mot.getTitre()%></td>
                <td><%=mot.getD1()%></td>
                <td><%=mot.getD2()%></td>
                
                <td><%=mot.getDatepublication()%><input type="text" name="publ<%=mot.getId()%>" value="<%=mot.getDatepublication().toString()%>"></td>
                <td><%=mot.getLieu()%></td>
                <td><%=mot.getDescri()%></td>
                <% for ( Photo photo : mot.getPhoto(dao) ) {%>
                <td> <img src="${pageContext.request.contextPath}/img/<%=photo.getPhoto()%>" height="100px" ></td>
                <% } %>
                <%if(mot.getEtat()==0){out.println("<td>Crée</td><td><a href='superAdmin?id="+mot.getId()+"'>Valider</a></td>");} 
                else{out.println("<td>Publié</td>");}%>
                <!--<td><input type="submit" formaction="superAdmin?identifiant=<%//= //mot.getId()%>" value="Modifier"></td>-->
                <td><a href="modifPubl?id=<%=mot.getId()%>">Modifier</a></td>
                
            </tr>
            <% } %>
                        </form>

        </table>
        
    </body>
</html>
