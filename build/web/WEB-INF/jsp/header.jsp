<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Princy
  Date: 28/01/2023
  Time: 12:41
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="fr">


    <!-- Mirrored from themesbrand.com/lexa/layouts/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 13 Jan 2023 07:47:17 GMT -->
    <head>

        <meta charset="utf-8" />
        <title>Dashboard | Lexa - Admin & Dashboard Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
        <meta content="Themesbrand" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/resources/assets/images/favicon.ico">

        <link href="<%= request.getContextPath()%>/resources/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<%= request.getContextPath()%>/resources/assets/css/app.min.css" rel="stylesheet" type="text/css" />
        <link href="<%= request.getContextPath()%>/resources/assets/css/icons.min.css" rel="stylesheet" type="text/css" />

        <link href="<%= request.getContextPath()%>/resources/assets/libs/dropzone/min/dropzone.min.css" rel="stylesheet" type="text/css" />
        <link href="<%= request.getContextPath()%>/resources/assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<%= request.getContextPath()%>/resources/assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css " rel="stylesheet">
        <link href="<%= request.getContextPath()%>/resources/assets/libs/spectrum-colorpicker2/spectrum.min.css " rel="stylesheet" type="text/css">
        <link href="<%= request.getContextPath()%>/resources/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css " rel="stylesheet" />
    </head>


    <body data-sidebar="dark">

        <!-- <body data-layout="horizontal" data-topbar="colored"> -->

        <!-- Begin page -->
        <div id="layout-wrapper">

            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="d-flex">
                        <!-- LOGO -->
                        <div class="navbar-brand-box">
                            <a href="<%= request.getContextPath()%>/index" class="logo logo-dark">
                                <span class="logo-sm">
                                    <img src="<%= request.getContextPath()%>/resources/assets/images/logo-sm.png" alt="" height="22">
                                </span>
                                <span class="logo-lg">
                                    <img src="<%= request.getContextPath()%>/resources/assets/images/logo-dark.png" alt="" height="17">
                                </span>
                            </a>

                            <a href="<%= request.getContextPath()%>/" class="logo logo-light">
                                <span class="logo-sm">
                                    <img src="<%= request.getContextPath()%>/resources/assets/images/logo-sm.png" alt="" height="22">
                                </span>
                                <span class="logo-lg">
                                    <img src="<%= request.getContextPath()%>/resources/assets/images/logo-light.png" alt="" height="18">
                                </span>
                            </a>
                        </div>

                    </div>

                    <div class="d-flex">

                        <!-- App Search-->
                        <form class="app-search d-none d-lg-block">
                            <div class="position-relative">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="fa fa-search"></span>
                            </div>
                        </form>

                        <div class="dropdown d-inline-block d-lg-none ms-2">
                            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="mdi mdi-magnify"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                                 aria-labelledby="page-header-search-dropdown">

                                <form class="p-3" style="margin-right: 500px;">
                                    <div class="form-group m-0">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                        <div class="dropdown d-none d-md-block ms-2">
                            <button type="button" class="btn header-item waves-effect" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="me-2" src="<%= request.getContextPath()%>/resources/assets/images/flags/us_flag.jpg" alt="Header Language" height="16"> English <span class="mdi mdi-chevron-down"></span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <img src="<%= request.getContextPath()%>/resources/assets/images/flags/italy_flag.jpg" alt="user-image" class="me-1" height="12"> <span class="align-middle"> Italian </span>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <img src="<%= request.getContextPath()%>/resources/assets/images/flags/french_flag.jpg" alt="user-image" class="me-1" height="12"> <span class="align-middle"> French </span>
                                </a>
                            </div>
                        </div>

                        <div class="dropdown d-none d-lg-inline-block">
                            <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                                <i class="mdi mdi-fullscreen font-size-24"></i>
                            </button>
                        </div>





                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                                <i class="mdi mdi-spin mdi-cog"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </header>
            <!-- ========== Left Sidebar Start ========== -->
            <div class="vertical-menu">

                <div data-simplebar class="h-100">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu list-unstyled" id="side-menu">
                            <li class="menu-title">Menu</li>

                            <li>
                                <a href="<%= request.getContextPath()%>/FormFilm" class="waves-effect">
                                    <i class="ion ion-md-add-circle-outline"></i>
                                    <span class="badge rounded-pill bg-primary float-end"></span>
                                    <span>Ajouter Film</span>
                                </a>
                            </li>

                            <li>
                                <a href="<%= request.getContextPath()%>/Front" class="waves-effect">
                                    <i class="ion ion-ios-film"></i>
                                    <span class="badge rounded-pill bg-primary float-end"></span>
                                    <span>Liste Film</span>
                                </a>
                            </li>

                            <li>
                                <a href="<%= request.getContextPath()%>/ChoixScene" class="waves-effect">
                                    <i class="ion ion-ios-albums"></i>
                                    <span class="badge rounded-pill bg-primary float-end"></span>
                                    <span>Planning</span>
                                </a>
                            </li>
                            
                            <li>
                                <a href="<%= request.getContextPath()%>/toFormAction" class="waves-effect">
                                    <i class="ion ion-md-videocam"></i>
                                    <span class="badge rounded-pill bg-primary float-end"></span>
                                    <span>Ajout Action</span>
                                </a>
                            </li>
                            <li>
                                <a href="<%= request.getContextPath()%>/MapPlateau" class="waves-effect">
                                    <i class="fas fa-map-marker-alt"></i>
                                    <span class="badge rounded-pill bg-primary float-end"></span>
                                    <span>Plateau</span>
                                </a>
                            </li>




                        </ul>
                    </div>
                    <!-- Sidebar -->
                </div>
            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">
                <div class="page-content">
                    <div class="container-fluid">
