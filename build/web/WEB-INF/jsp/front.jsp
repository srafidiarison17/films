<%-- 
    Document   : front
    Created on : 29 janv. 2023, 23:45:12
    Author     : Mitasoa
--%>

<%@page import="dao.HibernateDAO"%>
<%@page import="model.Photo"%>
<%@page import="java.util.List"%>
<%@page import="model.Publication"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Publication> mots = (List<Publication>) request.getAttribute("liste");
    HibernateDAO dao = (HibernateDAO)request.getAttribute("hibernate");
    String path = request.getAttribute("path") + "";
    double pg = (double)request.getAttribute("page");
    String m = (String)request.getAttribute("mot");
%>




<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Front Office</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/img/assets/bootstrap/css/bootstrap.min.css">
</head>

<body>
    <header>
        <h1>NEWS</h1>
        <form class="d-flex justify-content-center flex-wrap my-2"action="Front" method="GET">
            <div class="my-2"><input class="form-control"  name="mot" value="<%=m%>"></div>
            <div class="my-2"><button class="btn btn-primary ms-sm-2" type="submit">Recherche </button></div>
            </form>
        
    </header>
    
    <div class="card-group">

         <% for (Publication mot : mots) {%>
        <div class="card"> <% for ( Photo photo : mot.getPhoto(dao) ) {%>
            <div class="card"><img width="20px" class="card-img-top w-100 d-block" src="${pageContext.request.contextPath}/img/<%=photo.getPhoto()%>">
                <% } %>
            <div class="card-body">
                <p><small class="form-text">Crée le:<%=mot.getCreation()%></small></p>
                <h4 class="card-title" <%if(mot.getIdType()==2){out.print("style='color: blue'");}%>><%=mot.getTitre()%></h4>
                <p class="card-text"><%=mot.getDescri()%></p>
                <p><small class="form-text">Lieu:<%=mot.getLieu()%></small></p>
                <p><small class="form-text">Date deb:<%=mot.getD1()%></small></p>
                <p><small class="form-text">Date fin:<%=mot.getD2()%></small></p>
            </div>
        </div>
        </div><% } %></div>
    <p>
    <nav>
        <ul class="pagination">
             <% for(int i=1; i<=pg ; i++ ) { %>
            <li class="page-item"><a class="page-link" href="Front?mot=<%=m%>&nbr=<%=i%>"><%=i%></a>
            <% } %>
          
            <li class="page-item"><a class="page-link" aria-label="Next" href="#"><span aria-hidden="true">»</span></a></li>
        </ul>
    </nav></p>
    <script src="${pageContext.request.contextPath}/assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>