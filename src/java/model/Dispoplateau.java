
package model;

import annotation.AttributAnnotation;
import annotation.ClassAnnotation;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ClassAnnotation(table = "dispoplateau")

public class Dispoplateau {
 
             @AttributAnnotation(fieldname = "idplateau")

    private int idplateau;
    @AttributAnnotation(fieldname = "iddispo",primaryKey = true,autoIncrement = true)

             private int iddispo;
             @AttributAnnotation(fieldname = "dateind")

         private Date dateind;

    public int getIdplateau() {
        return idplateau;
    }

    public void setIdplateau(int idplateau) {
        this.idplateau = idplateau;
    }

    public int getIddispo() {
        return iddispo;
    }

    public void setIddispo(int iddispo) {
        this.iddispo = iddispo;
    }

    public Date getDateind() {
        return dateind;
    }

    public void setDateind(Date dateind) {
        this.dateind = dateind;
    }

    
   }
