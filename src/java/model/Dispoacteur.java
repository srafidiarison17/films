
package model;

import java.sql.Date;

public class Dispoacteur {

    private int idacteur;
    private int iddispo;
    private Date dateind;

    public int getIdacteur() {
        return idacteur;
    }

    public void setIdacteur(int idacteur) {
        this.idacteur = idacteur;
    }

    public int getIddispo() {
        return iddispo;
    }

    public void setIddispo(int iddispo) {
        this.iddispo = iddispo;
    }

    public Date getDateind() {
        return dateind;
    }

    public void setDateind(Date dateind) {
        this.dateind = dateind;
    }

    
    
}
