/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.AttributAnnotation;
import annotation.ClassAnnotation;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Rotsy
 */
 
 @ClassAnnotation(table = "dispPl")

public class DispPl {
         @AttributAnnotation(fieldname = "dateind")

    Date dateind;
             @AttributAnnotation(fieldname = "nom")

    String nom;
    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param idplateau the titre to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the dateind
     */
    public Date getDateind() {
        return dateind;
    }

    /**
     * @param dateind the titre to set
     */
    public void setDateind(Date dateind) {
        this.dateind = dateind;
    }

}
