package model;

import java.sql.Timestamp;
import java.sql.Time;
import java.sql.Date;

import java.util.Vector;

public class Planning {


	public static Vector get_planning(String[] id,Date debut,Date fin)throws Exception{
		
		Vector liste=new Vector();
		Scene[] scene=new Scene[id.length];
		String[] value=new String[1];
		String[] condition=new String[1];condition[0]="idscene";
		String[] signe=new String[1];signe[0]="=";
		Object[] object=null;
                int an=debut.getYear();
                int mois=debut.getMonth();
                int jour=debut.getDate();
		for (int i=0;i<id.length ;i++ ) {
			value[0]=id[i];
			object=Requete.select(new Scene(),condition,value,signe);
			scene[i]=(Scene)object[0];
			Tournage tournage=Planning.controle_date(scene[i],debut,fin,new Date(an,mois,jour),liste);
			if( tournage!=null ){ liste.addElement(tournage); }
		}
		return liste;
	}

	public static int gethours(long a){
		int i=0;
		while(a<3600){
			i++;
			a=a-3600;
		}
		return i;
	}
	public static int getminutes(long a){
		int i=0;
		while(a<60){
			i++;
			a=a-60;
		}
		return i;
	} 


	public static Tournage controle_date(Scene scene,Date debut,Date fin,Date date,Vector liste)throws Exception{

		Tournage tournage=null;
		if (date.getDate()>fin.getDate()) { return tournage; }
		long heure=(scene.getHorairedebut().getHours()*3600)+(scene.getHorairedebut().getMinutes()*60)+scene.getHorairedebut().getSeconds();
		long duree=(scene.getDuree().getHours()*3600)+((scene.getDuree().getMinutes()+30)*60)+scene.getDuree().getSeconds();
		String[] condition=new String[1];condition[0]="idscene";
		String[] value=new String[1];value[0]=""+scene.getIdscene();
		String[] signe=new String[1];signe[0]="=";
		Object[] object=Requete.select(new Action(),condition,value,signe);
		Action[] actions=new Action[object.length];
		for (int i=0;i<object.length ;i++ ) { actions[i]=(Action)object[i]; }
		long a=Planning.controle_heure(scene,actions,date,liste,heure,duree);
		if ( a==-1 ) { 
			date.setDate(date.getDate()+1);
			return Planning.controle_date(scene,debut,fin,date,liste); 
		}
		if ( a!=-1 ) {
			int h=Planning.gethours(a);
			int m=Planning.gethours(a-(h*3600));
			int s=Integer.parseInt(new Long(a-(h*3600)-(m*60)).toString());
			Timestamp debut_tournage=new Timestamp(date.getYear(),date.getMonth(),date.getDate(),h,m,s,0);
			a=a+duree;
			h=Planning.gethours(a);
			m=Planning.gethours(a-(h*3600));
			s=Integer.parseInt(new Long(a-(h*3600)-(m*60)).toString());
			Timestamp fin_tournage=new Timestamp(date.getYear(),date.getMonth(),date.getDate(),h,m,s,0);
			tournage=new Tournage();
			tournage.setDebut(debut_tournage);
			System.out.println(debut_tournage);
			tournage.setFin(fin_tournage);
			System.out.println(fin_tournage);
			tournage.setIdscene(scene.getIdscene());
//                        tournage.scene=scene;
//			System.out.println(scene.getIdscene());
		}
		return tournage;                                
	}


	public static long controle_heure(Scene scene,Action[] actions,Date date,Vector liste,long heure,long duree)throws Exception{
		
		if (Planning.controle_plateau(scene.getIdplateau(),date)==false||Planning.controle_acteur(scene,date)==false||Planning.controle_ferie(date)==false||Planning.controle_weekend(date)==false) { return -1; }
		long fin=(scene.getHorairefin().getHours()*3600)+(scene.getHorairefin().getMinutes()*60)+scene.getHorairefin().getSeconds();
		long a=Planning.controle_liste(liste,heure,date,duree,scene.getIdplateau(),actions);
		long b=Planning.controle_scene(heure,date,duree,scene.getIdplateau(),actions);
		if (b!=0) { return Planning.controle_heure(scene,actions,date,liste,b,duree); }
		if (a==0) {
			if ( (heure+duree)<=fin ){ return heure; } 
			if ( (heure+duree)>fin ) { return -1; }
		}
		return Planning.controle_heure(scene,actions,date,liste,a,duree);

	}


	public static long controle_scene(long heure,Date date,long duree,int idplateau,Action[] actions)throws Exception{

		String[] condition=new String[1];condition[0]="etat";
		String[] value=new String[1];value[0]="2";
		String[] signe=new String[1];signe[0]="=";
		Object[] object_scene=Requete.select(new Scene(),condition,value,signe);
		Scene scene=null;
		Object[] object_action=null;
		Object[] object_tournage=null;
		Action action=null;
		Tournage tournage=null;
		long debut=0;
		long fin=0;

		for (int i=0;i<object_scene.length ;i++ ) {
			scene=(Scene)object_scene[i];
			condition[0]="idscene";
			value[0]=""+scene.getIdscene();
			object_action=Requete.select(new Action(),condition,value,signe);
			for (int x=0;x<object_action.length ;x++ ) {
				action=(Action)object_action[x];
				for (int y=0;y<actions.length ;y++ ) {
					if (action.getIdacteur()==actions[y].getIdacteur()) {
                                            debut=(scene.getHorairedebut().getHours()*3600)+(scene.getHorairedebut().getMinutes()*60)+scene.getHorairedebut().getSeconds();
                                            fin=(scene.getHorairefin().getHours()*3600)+(scene.getHorairefin().getMinutes()*60)+scene.getHorairefin().getSeconds();
                                            if ( (debut<=heure&&heure<fin) || (debut<(heure+duree)&&(heure+duree)<=fin) || (debut>heure&&fin<(heure+duree)) ) {
						return fin;
                                            }	
                                        }
				}
			}
		}

		for (int i=0;i<object_scene.length ;i++ ) {
			
			scene=(Scene)object_scene[i];
			condition[0]="idscene";
			value[0]=""+scene.getIdscene();
			object_tournage=Requete.select(new Tournage(),condition,value,signe);
			tournage=(Tournage)object_tournage[0];


			if(scene.getIdplateau()==idplateau){
				if (tournage.getDebut().getDate()==date.getDate()&&tournage.getDebut().getMonth()==date.getMonth()&&tournage.getDebut().getYear()==date.getYear()) {
					debut=(tournage.getDebut().getHours()*3600)+(tournage.getDebut().getMinutes()*60)+tournage.getDebut().getSeconds();
					fin=(tournage.getFin().getHours()*3600)+(tournage.getFin().getMinutes()*60)+tournage.getFin().getSeconds();
					if ( (debut<=heure&&heure<fin) || (debut<(heure+duree)&&(heure+duree)<=fin) || (debut>heure&&fin<(heure+duree)) ) {
						return fin;
					}
				}
			}
		
		}
		return 0;

	}


	public static long controle_liste(Vector liste,long heure,Date date,long duree,int idplateau,Action[] actions)throws Exception{

		if(liste.size()==0){ return 0; }
		
		Tournage tournage=null;
		String[] condition=new String[1];condition[0]="idscene";
		String[] value=new String[1];
		String[] signe=new String[1];signe[0]="=";
		Object[] object_tournage=null;
		Object[] object_action=null;
		Action action=null;
		Scene scene=null;
		long debut=0;
		long fin=0;
		
		for (int i=0;i<liste.size() ;i++ ) {

			tournage=(Tournage)liste.get(i);
			value[0]=""+tournage.getIdscene();
			object_action=Requete.select(new Action(),condition,value,signe);
			for (int x=0;x<object_action.length ;x++ ) {
				action=(Action)object_action[x];
				for (int y=0;y<actions.length ;y++ ) {
					if (action.getIdacteur()==actions[y].getIdacteur()) {
                                            debut=(tournage.getDebut().getHours()*3600)+(tournage.getDebut().getMinutes()*60)+tournage.getDebut().getSeconds();
                                            fin=(tournage.getFin().getHours()*3600)+(tournage.getFin().getMinutes()*60)+tournage.getFin().getSeconds();
                                            if ( (debut<=heure&&heure<fin) || (debut<(heure+duree)&&(heure+duree)<=fin) || (debut>heure&&fin<(heure+duree)) ) {
						return fin;
                                            }
                                        }
				}
			}
			
		}


		for (int i=0;i<liste.size() ;i++ ) {
			
			tournage=(Tournage)liste.get(i);
			value[0]=""+tournage.getIdscene();
			object_tournage=Requete.select(new Scene(),condition,value,signe);
			scene=(Scene)object_tournage[0];


			if(scene.getIdplateau()==idplateau){
				if (tournage.getDebut().getDate()==date.getDate()&&tournage.getDebut().getMonth()==date.getMonth()&&tournage.getDebut().getYear()==date.getYear()) {
					debut=(tournage.getDebut().getHours()*3600)+(tournage.getDebut().getMinutes()*60)+tournage.getDebut().getSeconds();
					fin=(tournage.getFin().getHours()*3600)+(tournage.getFin().getMinutes()*60)+tournage.getFin().getSeconds();
					if ( (debut<=heure&&heure<fin) || (debut<(heure+duree)&&(heure+duree)<=fin) || (debut>heure&&fin<(heure+duree)) ) {
						return fin;
					}
				}
			}
		
		}
		return 0;

	}

	public static boolean controle_plateau(int idplateau,Date date)throws Exception{

		String[] condition=new String[1];condition[0]="idplateau";
		String[] value=new String[1];value[0]=""+idplateau;
		String[] signe=new String[1];signe[0]="=";
		Object[] object=Requete.select(new Dispoplateau(),condition,value,signe);
		Dispoplateau dispo=null;
		for (int i=0;i<object.length ;i++ ) { 
			dispo=(Dispoplateau)object[i]; 
			if (dispo.getDateind().getDate()==date.getDate()&&dispo.getDateind().getMonth()==date.getMonth()&&dispo.getDateind().getYear()==date.getYear()) {
				return false;
			}
		}
		return true;

	}

public static boolean controle_acteur(Scene scene,Date date)throws Exception{

		String[] condition=new String[1];condition[0]="idscene";
		String[] value=new String[1];value[0]=""+scene.getIdscene();
		String[] signe=new String[1];signe[0]="=";
		Object[] object_action=Requete.select(new Action(),condition,value,signe);
		Object[] object_indispo=null;
		Action action=null;
		Dispoacteur dispo=null;
		for (int i=0;i<object_action.length ;i++ ) { 
			action=(Action)object_action[i]; 
			condition[0]="idacteur";
			value[0]=""+action.getIdacteur();
			object_indispo=Requete.select(new Dispoacteur(),condition,value,signe);
			for (int x=0;x<object_indispo.length ;x++ ) {
				dispo=(Dispoacteur)object_indispo[x];
				if (dispo.getDateind().getDate()==date.getDate()&&dispo.getDateind().getMonth()==date.getMonth()&&dispo.getDateind().getYear()==date.getYear()) {
					return false;
				}
			}
		}
		return true;
	}

public static boolean controle_ferie(Date date)throws Exception{

		Object[] object=Requete.select(new Ferie(),new String[0],new String[0],new String[0]);
		Ferie jour=null;
		for (int i=0;i<object.length ;i++ ) { 
			jour=(Ferie)object[i]; 
			if (jour.getJour().getDate()==date.getDate()&&jour.getJour().getMonth()==date.getMonth()&&jour.getJour().getYear()==date.getYear()) {
				return false;
			}
		}
		return true;

	}

	public static boolean controle_weekend(Date date)throws Exception{
		if (date.getDay()==0||date.getDay()==6) {
			return false;
		}
		return true;
	}
	public static void main(String[] args) throws Exception {
		String[] s=new String[3];s[0]="21";s[1]="22";s[2]="23";
		Vector liste=Planning.get_planning(s,new Date(2023-1900,2-1,1),new Date(2023-1900,2-1,7));
                
                
	}

}