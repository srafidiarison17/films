package model;

import annotation.AttributAnnotation;
import annotation.ClassAnnotation;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ClassAnnotation(table = "plateau")
@Entity
public class Plateau {

    @AttributAnnotation(fieldname = "nom")
    private String nom;

    @AttributAnnotation(fieldname = "idplateau")
    @Id
    @Column(name = "idplateau", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idplateau;

    @AttributAnnotation(fieldname = "longitude")
    private double longitude;

    @AttributAnnotation(fieldname = "lattitude")
    private double lattitude;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLattitude() {
        return lattitude;
    }

    public void setLattitude(double lattitude) {
        this.lattitude = lattitude;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getIdplateau() {
        return idplateau;
    }

    public void setIdplateau(int idplateau) {
        this.idplateau = idplateau;
    }

}
