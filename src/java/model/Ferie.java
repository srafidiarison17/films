
package model;

import java.sql.Date;

public class Ferie {

    private int idFerie;
    private Date jour;

    public int getIdFerie() {
        return idFerie;
    }

    public void setIdFerie(int idFerie) {
        this.idFerie = idFerie;
    }

    public Date getJour() {
        return jour;
    }

    public void setJour(Date jour) {
        this.jour = jour;
    }

    
    
}
