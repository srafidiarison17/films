/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.persistence.Entity;

/**
 *
 * @author Mitasoa
 */
@Entity
public class Photo extends BaseModel {
    private int idPublication;
    private String photo;

    /**
     * @return the idPublication
     */
    public int getIdPublication() {
        return idPublication;
    }

    /**
     * @param idPublication the idPublication to set
     */
    public void setIdPublication(int idPublication) {
        this.idPublication = idPublication;
    }

    /**
     * @return the photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * @param photo the photo to set
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
