
package model;

import annotation.AttributAnnotation;
import annotation.ClassAnnotation;
import javax.persistence.Entity;
import java.sql.Time;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
//@ClassAnnotation(table = "scene")
public class Scene {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idscene")
    private int idscene;

     public int getIdscene() {
        return idscene;
    }

    public void setIdscene(int idscene) {
        this.idscene = idscene;
    }

    private String nom;
    private String auteur;

    private Time duree;

    private Time horairedebut;

    private Time horairefin;

    private int idplateau;

    private int idfilm;

    private int etat;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Time getDuree() {
        return duree;
    }

    public void setDuree(Time duree) {
        this.duree = duree;
    }

    public Time getHorairedebut() {
        return horairedebut;
    }

    public void setHorairedebut(Time horairedebut) {
        this.horairedebut = horairedebut;
    }

    public Time getHorairefin() {
        return horairefin;
    }

    public void setHorairefin(Time horairefin) {
        this.horairefin = horairefin;
    }

    public int getIdplateau() {
        return idplateau;
    }

    public void setIdplateau(int idplateau) {
        this.idplateau = idplateau;
    }

    public int getIdfilm() {
        return idfilm;
    }

    public void setIdfilm(int idfilm) {
        this.idfilm = idfilm;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    
}
