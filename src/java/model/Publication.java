/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.HibernateDAO;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Mitasoa
 */
@Entity
public class Publication extends BaseModel {
    private int idType;
    private String titre;
    @Column(name="date1")
    private Date d1;
    @Column(name="date2")
    private Date d2;
    private String lieu;
    @Column(name="description")
    private String descri;
    //@Column(name="datepublication")
    private Timestamp datepublication;          
    
    @Column(name="creation")
    private Date creation;          
    
    @Column(name="etat")
    private int etat;          
    
        @Column(name="visibility")

    private int visibility;
    /**
     * @return the idType
     */
    public int getIdType() {
        return idType;
    }

    /**
     * @param idType the idType to set
     */
    public void setIdType(int idType) {
        this.idType = idType;
    }

    /**
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * @param titre the titre to set
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * @return the d1
     */
    public Date getD1() {
        return d1;
    }

    /**
     * @param d1 the d1 to set
     */
    public void setD1(Date d1) {
        this.d1 = d1;
    }

    /**
     * @return the d2
     */
    public Date getD2() {
        return d2;
    }

    /**
     * @param d2 the d2 to set
     */
    public void setD2(Date d2) {
        this.d2 = d2;
    }

    /**
     * @return the lieu
     */
    public String getLieu() {
        return lieu;
    }

    /**
     * @param lieu the lieu to set
     */
    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    /**
     * @return the descri
     */
    public String getDescri() {
        return descri;
    }

    /**
     * @param descri the descri to set
     */
    public void setDescri(String descri) {
        this.descri = descri;
    }
    
    
    public Timestamp getDatepublication() {
        return datepublication;
    }

    public void setDatepublication(Timestamp datepublication) {
        this.datepublication = datepublication;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Date getCreation() {
        return creation;
    }
        
    public void setCreation(Date creation) {
        this.creation = creation;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }
    
    /**
     * @return the photo
     */
    public Photo [] getPhoto(HibernateDAO dao) {
        Photo p = new Photo();
        p.setIdPublication(Integer.parseInt(this.getId()+""));
        Photo [] __photo = new Photo[dao.findWhere(p).size()];
//        System.out.println(dao.findWhere(p).size());
        for (int i = 0; i < dao.findWhere(p).size(); i++) {
            __photo[i] = dao.findWhere(p).get(i);
        }
        return __photo;
    }
}
