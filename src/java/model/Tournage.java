
package model;

import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Tournage {

    private int idtournage;
    private int idscene;
    private Timestamp debut;
    private Timestamp fin;
    
    
    public Scene get_scene(){
        String[] value=new String[1];value[0]=""+this.getIdscene();
	String[] condition=new String[1];condition[0]="idscene";
	String[] signe=new String[1];signe[0]="=";
        Object[] object = null;
        try {
            object = Requete.select(new Scene(),condition,value,signe);
        } catch (Exception ex) {
            Logger.getLogger(Tournage.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (Scene)object[0];
    }

  
    public int getIdtournage() {
        return idtournage;
    }

    public void setIdtournage(int idtournage) {
        this.idtournage = idtournage;
    }

    public int getIdscene() {
        return idscene;
    }

    public void setIdscene(int idscene) {
        this.idscene = idscene;
    }

    public Timestamp getDebut() {
        return debut;
    }

    public void setDebut(Timestamp debut) {
        this.debut = debut;
    }

    public Timestamp getFin() {
        return fin;
    }

    public void setFin(Timestamp fin) {
        this.fin = fin;
    }

    
}
