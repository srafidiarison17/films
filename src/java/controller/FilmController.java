/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import dao.HibernateDAO;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import model.Acteur;
import model.Action;
import model.Film;
import model.Plateau;
import model.Scene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import dao.*;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import model.DispPl;
import model.Dispoplateau;
import model.Planning;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.ui.Model;
/**
 *
 * @author ASUS
 */
@Controller

public class FilmController {

    @Autowired
    private HibernateDAO dao;

    private ModelAndView modelandview = new ModelAndView();

    public static String changeToTime(String dateString) throws Exception {
        SimpleDateFormat inputFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm:ss");
        java.util.Date parsedDate = inputFormat.parse(dateString);
        String formattedDate = outputFormat.format(parsedDate);
        return formattedDate;
    }

    @RequestMapping("/Front")
    public ModelAndView index() {
        getModelandview().addObject("film", getDao().findAll(Film.class));
        getModelandview().setViewName("ListeFilm");
        return getModelandview();
    }

    @RequestMapping(value = "/toFormAction")
    public ModelAndView toFormAction() {
        getModelandview().addObject("scene", getDao().findAll(Scene.class));
        getModelandview().addObject("acteur", getDao().findAll(Acteur.class));
        getModelandview().setViewName("FormAction");
        return getModelandview();
    }

    @RequestMapping(value = "/insertIndis", method = RequestMethod.POST)
    public String insertIndis(HttpServletRequest req, HttpSession ses) throws Exception {
        String idPlateau = req.getParameter("idPlateau");
        String dateinds = req.getParameter("dateinds");
        SimpleDateFormat htmlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date d = htmlDateFormat.parse(dateinds);

        Dispoplateau nd = new Dispoplateau();
        
        nd.setIdplateau(Integer.parseInt(idPlateau));
        nd.setDateind(d);
        GenericDAO.save(nd);
//        dao.create(nd);
        
        return "redirect:liste";
    }

    @RequestMapping(value = "/datesDispo", method = RequestMethod.POST)
    public String datesDispo(Model mod, HttpServletRequest req, HttpSession ses) throws Exception {
        String nomPlateau = req.getParameter("nomPlateau");

        ArrayList<DispPl> allPl = new ArrayList();
    for(Object obj:GenericDAO.getAll(new DispPl()))
    {
        allPl.add((DispPl)obj);
    }
        ArrayList<DispPl> dp = new ArrayList<>();
        
        for(int i =0; i<allPl.size();i++){
            if(allPl.get(i).getNom().equals(nomPlateau)){
                dp.add(allPl.get(i));
            }
        }
        
        ArrayList<Plateau> allPlateau = new ArrayList<>();
        for(Object obj:GenericDAO.getAll(new Plateau()))
    {
        allPlateau.add((Plateau)obj);
    }
        mod.addAttribute("allPlateau", allPlateau);
        mod.addAttribute("dp", dp);

        return "DatesDis";
    }
    @RequestMapping(value = "/insertAction", method = RequestMethod.POST)
    public ModelAndView insertAction(HttpServletRequest req, HttpSession ses) {
        int idscene = Integer.parseInt(req.getParameter("idscene"));
        int idacteur = Integer.parseInt(req.getParameter("idacteur"));
        String sentiment = req.getParameter("sentiment");
        String dialogue = req.getParameter("dialogue");
        String action = req.getParameter("action");
        int ordre = Integer.parseInt(req.getParameter("ordre"));
        int duree = Integer.parseInt(req.getParameter("duree"));

        Action a = new Action();
        a.setAction(action);
        a.setDialogue(dialogue);
        a.setIdacteur(idacteur);
        a.setSentiment(sentiment);
        a.setOrdre(ordre);
        a.setDuree(duree);
        a.setIdscene(idscene);

        getDao().create(a);
        getModelandview().setViewName("FormAction");
        return getModelandview();

    }

    @RequestMapping(value = "/FormScene/{id}", method = RequestMethod.GET)
    public ModelAndView PageScene(@PathVariable int id, HttpServletRequest request) {
        System.out.println("id du film" + id);
        HttpSession session = request.getSession();
        session.setAttribute("idfilm", id);
        Scene scene = new Scene();
        scene.setIdfilm(id);
        System.out.println("ID " + scene.getIdfilm());
        try {
            Connection con=Connexion.connectDB();
             getModelandview().addObject("plateau", GenericDAO.getAll(new Plateau()));
             getModelandview().addObject("scene", GenericDAO.getAll(scene));
                    
        } catch (Exception e) {
            e.printStackTrace();
        }
//        getModelandview().addObject("plateau", getDao().findAll(Plateau.class));
//        getModelandview().addObject("scene", getDao().findWhere(scene));
        getModelandview().setViewName("FormScene");
        return getModelandview();
    }

    @RequestMapping(value = "/AjoutScene", method = RequestMethod.POST)
    public ModelAndView AjoutScene(HttpServletRequest request) throws Exception {
        getModelandview().setViewName("ListeFilm");
        HttpSession session = request.getSession();
        int idfilm = (int) session.getAttribute("idfilm");
        if (idfilm > 0) {
            Scene scene = new Scene();
            scene.setIdplateau(Integer.parseInt(request.getParameter("idplateau")));
            scene.setNom(request.getParameter("nom"));
            scene.setDuree(Time.valueOf(changeToTime(request.getParameter("duree"))));
            scene.setIdfilm(idfilm);
            getDao().create(scene);
            //session.removeAttribute("idfilm");
        }
        return index();
    }

    @RequestMapping(value = "/recherche", method = RequestMethod.GET)
    public ModelAndView Recherche(HttpServletRequest request) {
        Film film = new Film();
        film.setTitre(request.getParameter("titre"));
        getModelandview().addObject("film", getDao().findWhere(film));
        getModelandview().setViewName("ListeFilm");
        return getModelandview();
    }

    @RequestMapping(value = "/FormFilm")
    public ModelAndView toFormFilm() {
        getModelandview().setViewName("FormFilm");
        return getModelandview();
    }

//    @RequestMapping(value = "/insertFilm", method = RequestMethod.POST)
//    public String insertFilm(HttpServletRequest req, HttpSession ses) {
//        String titre = req.getParameter("titre");
//        String auteur = req.getParameter("auteur");
//        String couverture = req.getParameter("couverture");
//        String time = req.getParameter("duree");
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
//        LocalTime lt = LocalTime.parse(time, formatter);
//        Time t = Time.valueOf(lt);
//
//        Film newFilm = new Film();
//
//        newFilm.setTitre(titre);
//        newFilm.setAuteur(auteur);
//        newFilm.setCouverture(couverture);
//        newFilm.setDuree(t);
//        getDao().create(newFilm);
//        return "ListeFilm";
//    }

    

    @RequestMapping(value = "/AjoutFilm", method = RequestMethod.POST)
    public ModelAndView upload(@RequestParam CommonsMultipartFile photo, HttpSession session, HttpServletRequest request) {
        String path = session.getServletContext().getRealPath("/");
        String filename = photo.getOriginalFilename();
        System.out.println("NAME " + filename );
        try {
            byte barr[] = photo.getBytes();
            String nomPhoto = path + "img\\" + filename;
            System.out.println("upload : " + nomPhoto);
            BufferedOutputStream bout = new BufferedOutputStream(
                    new FileOutputStream(nomPhoto));
            bout.write(barr);
            bout.flush();
            bout.close();
            Film film = new Film();
            film.setCouverture(filename);
            film.setDuree(Time.valueOf(changeToTime(request.getParameter("duree"))));
            film.setTitre(request.getParameter("titre"));
            film.setAuteur(request.getParameter("auteur"));
            dao.create(film);

        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }
        return index();
    }
@RequestMapping(value = "/MapPlateau")
    public ModelAndView toMapPlateau() {
        getModelandview().addObject("plateau", getDao().findAll(Plateau.class));
        getModelandview().setViewName("ListePlateau");
        return getModelandview();
    }

    @RequestMapping(value = "/SceneIDplateau/{id}", method = RequestMethod.GET)
    public void getSceneIDPlateau(@PathVariable int id, HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println(id);
        JSONArray array = new JSONArray();
        PrintWriter out = response.getWriter();
        try {
            Scene scene = new Scene();
            scene.setIdplateau(id);
            List<Scene> list = getDao().findWhere(scene);
            String retour = "";
            for (Scene s : list) {
                JSONObject obj = new JSONObject();
                obj.put("nom", s.getNom());
                obj.put("duree", s.getDuree().toString());
                obj.put("idfilm", s.getIdfilm()+"");
                array.add(obj.toJSONString());
                retour += obj + ",";
            }
            out.println(array);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
//            Logger.getLogger(FilmController.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    //Kaloina debut
    
    
     @RequestMapping("/ChoixScene")
    public ModelAndView choixscene() {
/*
        ArrayList<Scene> ls =new ArrayList<Scene>();
        Scene s=new Scene();
        s.setNom("scene13");
        ls.add(s);
        ls.add(s);
        ls.add(s);
        ls.add(s);*/
        Scene scene = new Scene();
        scene.setEtat(3);
        getModelandview().addObject("scene", getDao().findAll(Scene.class));
        //getModelandview().addObject("scene", ls);
        //getModelandview().addObject("film", getDao().findAll(Film.class));

        getModelandview().setViewName("ChoixScene");
        return getModelandview();
    }
    
    @RequestMapping(value = "/SuggestPlan", method = RequestMethod.GET)
    public ModelAndView SuggestPlan(HttpServletRequest request, HttpSession session,HttpServletResponse res) {
        String deb=(String)request.getParameter("debut");
        String fin=(String)request.getParameter("fin");
        String[] scenes=(String[])request.getParameterValues("scenes");
      
        try {
            for (int i = 0; i < scenes.length; i++) {
            System.out.println(scenes[i]);                
            }
            Vector liste=Planning.get_planning(scenes,java.sql.Date.valueOf(deb),java.sql.Date.valueOf(fin));
                    getModelandview().addObject("liste", liste);

//            System.out.println(liste);
        } catch (Exception ex) {
            ex.printStackTrace();
//            Logger.getLogger(FilmController.class.getName()).log(Level.SEVERE, null, ex);
        }

        /*
        String[] ls=(String[])request.getParameterValues("scenes");
        String st=ls[1];*/
        getModelandview().addObject("test", deb);
        getModelandview().addObject("test1", fin);
        //System.out.println( "test "+getModelandview().getModel().get("test"));
        getModelandview().setViewName("SuggestPlan");
                
        return getModelandview();
    }
    
    
    //Kaloina fin
    
    /**
     * @return the dao
     */
    public HibernateDAO getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(HibernateDAO dao) {
        this.dao = dao;
    }

    /**
     * @return the modelandview
     */
    public ModelAndView getModelandview() {
        return modelandview;
    }

    /**
     * @param modelandview the modelandview to set
     */
    public void setModelandview(ModelAndView modelandview) {
        this.modelandview = modelandview;
    }
}
