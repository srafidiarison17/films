<%@page import="java.util.List"%>
<%@page import="model.Film"%>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
 <%-- Created by IntelliJ IDEA.
  User: Princy
  Date: 28/01/2023
  Time: 12:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"></jsp:include>



    <div class="row">
            <div class="card">
                <div class="card-body">
                    <div id="carouselExampleCaption" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img style="width: 1500px;height: 300px;"  src="<%= request.getContextPath() %>/resources/assets/images/front.jpg" >
                                <div class="carousel-caption d-none d-md-block">
                                    <h5 class="text-white">Planning des films</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.o.
                                        Pellentesque vulputate placerat dolor,
                                        congue egestas ipsum euismod a.
                                        </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>



<%
    if(request.getAttribute("info")!=null){
        String info = request.getAttribute("info").toString();%>
    <div style="width: 400px;margin-left: 500px;" class="alert alert-info alert-dismissible fade show" role="alert">
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
        </button>
        <strong>Info: </strong> <%= info %>
    </div>
<%   } %>


<div class="card" style="width: 300px;float: left;">
    <div class="card-body">
        <div class="row">
            <form action="<%= request.getContextPath() %>/recherche" method="get" >
                <div class="mb-3">
                    <label class="form-label">Titre</label>
                        <input data-parsley-type="alphanum" name="titre" type="text"
                               class="form-control"
                               placeholder="Titre"/>
                     <br/>
                     <br/>
                    <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                        Searching
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row" style="clear: right;">
    <%
        List<Film> li = (List<Film>) request.getAttribute("film");
    %>
    <%
        for(Film f : li){
    %>
                <div class="col-lg-4">
                    <div class="card">
                        <img class="card-img-top img-fluid" src="<%= request.getContextPath() %>/img/<%= f.getCouverture() %>" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title"><%= f.getTitre() %></h4>
                            <p>Durée  : <%= f.getDuree() %> </p>

                            <p class="card-text">
                            <p style="color: #7b70be;font-size: 12px;">Auteur : <%= f.getAuteur() %> </p>
                            </p>

                            <br>
               
                            <a href="<%= request.getContextPath() %>/FormScene/<%= f.getId() %>" class="btn btn-outline-primary waves-effect waves-light">
                                Ajouter scène
                            </a>

                        </div>
                    </div>
                </div>
                                <%
                                    }
                                %>
</div>


<nav style="margin-left: 500px;" aria-label="Page navigation example">
    <ul class="pagination">
        <li class="page-item"><a class="page-link" href="<%= request.getContextPath() %>/paginate/1">1</a></li>
        <li class="page-item"><a class="page-link" href="<%= request.getContextPath() %>/paginate/2">2</a></li>
        <li class="page-item"><a class="page-link" href="<%= request.getContextPath() %>/paginate/3">3</a></li>
    </ul>
</nav>


<jsp:include page="footer.jsp"></jsp:include>