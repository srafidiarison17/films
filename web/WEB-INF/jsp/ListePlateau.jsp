<%-- 
    Document   : ListePlateau
    Created on : 8 mars 2023, 15:23:15
    Author     : Princy
--%>

<%@page import="model.Plateau"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>

<%
    List<Plateau> liste = (List<Plateau>) request.getAttribute("plateau");
%>

<script type="text/javascript"      
        src="http://maps.google.com/maps/api/js?sensor=false">
</script>

<script type="text/javascript">
    function initialize() {
        var mapOptions = {
            center: new google.maps.LatLng(-18.933333, 47.516667),
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var carte = new google.maps.Map(document.getElementById("carteId"), mapOptions);

        //Position  
        var coord = [<% int id = 1;
            for (Plateau p : liste) {%>        [            <%= id%>, new google.maps.LatLng(<%= p.getLattitude()%>, <%= p.getLongitude()%>), '<%= p.getNom()%>'        ],
    <%
            id++;
        }
    %>
        ];

        // Création d'un marqueur
        for (var index = 0; index < coord.length; index++) {
            const id = coord[index][0];
            const element = coord[index][1];
            const nom = coord[index][2];

            var marker = new google.maps.Marker({
                position: element,
                map: carte,
                label: {
                    text: nom,
                    color: "blue"
                }
            });

            // Ajouter un événement de clic à chaque marqueur
            google.maps.event.addListener(marker, 'click', (function (id) {
                return function () {
                    displayTable(id);
                }
            })(id));
        }
    }
    function displayTable(id) {
        
        let table = document.getElementById("table-id");
        var testTable=  $('#table-id');
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/FilmVrai/SceneIDplateau/" + id,
            datatype: "json",
            success: function (data, textStatus, jqXHR) {
                console.log("data= " + data);
                var liste = JSON.parse(data);
                console.log(liste);
//              testTable.empty();ù
              $("#table-id tr:not(:first)").remove();
//                                  $("#myTable tr").remove();  

                $.each(liste, function (i, items) {
                    var plateau = JSON.parse(items);
                    console.log('HEE: ' + plateau.nom);
                    // Crée une nouvelle ligne dans la table HTML
                    let newRow = table.insertRow();

                    // Ajoute les cellules pour chaque colonne
                    let nomCell = newRow.insertCell();
                    nomCell.innerHTML = plateau.nom;

                    let dureeCell = newRow.insertCell();
                    dureeCell.innerHTML = plateau.duree;

                    let idfilmCell = newRow.insertCell();
                    idfilmCell.innerHTML = plateau.idfilm;

                });
            },

            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>



<div class="row" >
    <div class="card" >
        <div class="card-body" >

            <h4 class="card-title">Plateaux</h4>
            <p class="card-title-desc">Liste de tous les plateaux de tournage</p>
            <div  id="carteId" class="gmaps"></div>

        </div>
    </div>

    <!-- response -->
    <div class="card" >
        <div class="card-body" >
            <div id="datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="table-id" class="table table-bordered dt-responsive nowrap dataTable no-footer dtr-inline" style="border-collapse: collapse; border-spacing: 0px; width: 100%;" role="grid" aria-describedby="datatable_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 289.6px;" aria-label="Position: activate to sort column ascending">Scene</th>
                                    <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 289.6px;" aria-label="Position: activate to sort column ascending">Duree</th>
                                    <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 143.6px;" aria-label="Office: activate to sort column ascending">IdFilm</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>
