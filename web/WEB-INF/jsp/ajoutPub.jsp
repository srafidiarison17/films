<%-- 
    Document   : ajoutPub
    Created on : 29 janv. 2023, 22:21:13
    Author     : Mitasoa
--%>

<%@page import="model.Publication"%>
<%@page import="model.Type"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Type> types = (List<Type>) request.getAttribute("type");
    List<Publication> pubs = (List<Publication>) request.getAttribute("pub");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ajout de publucation</title>
            <link rel="stylesheet" href="${pageContext.request.contextPath}/img/assets/bootstrap/css/bootstrap.min.css">

    </head>
    <body>
        <h1>Back office</h1>
        <form method="POST">
            <label>Type</label>
            <select name="idType" >
                <%
                    for (Type type : types) {%>
                <option value="<%= type.getId()%>"><%= type.getNom()%></option>
                <% } %>
            </select>
            <p><label>Titre</label></p>
            <input type="text" name="titre">
            <p><label>Date 1</label></p>
            <input type="date" name="d1">
            <p><label>Date 2</label></p>
            <input type="date" name="d2">
            <p><label>Date de publication</label></p>
            <input type="date" name="datepubl">
            <input type="time" name="publ">
            
            <p><label>Lieu</label> </p>
            <input type="text" name="lieu">
            <label>Decription</label>
            <input type="text" name="descri">
            <input type="submit" name="">
        </form>
    </body>
    <hr/>
    <hr/>
    <hr/>
    <h2>Upload photo </h1>
    <label>Publication</label>
    <form action="AjoutPhoto" method="post" enctype="multipart/form-data">  
        <select name="idPub" > 
            <%
            for (Publication pub : pubs) {%>
            <option value="<%= pub.getId()%>"><%= pub.getTitre()%></option>
            <% }%>
        </select>
        Selectionner une photo: <input type="file" name="photo"/>  
        <input type="submit" value="AJOUTER"/>  
    </form>  
</html>
