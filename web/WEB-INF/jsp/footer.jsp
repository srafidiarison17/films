<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>


</div>
<!-- container-fluid -->
</div>
<!-- End Page-content -->

<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                © <script>document.write(new Date().getFullYear())</script> Lexa <span class="d-none d-sm-inline-block"> - Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesbrand.</span>
            </div>
        </div>
    </div>
</footer>
</div>
<!-- end main content-->

</div>
<!-- END layout-wrapper -->

<!-- Right Sidebar -->
<div class="right-bar">
    <div data-simplebar class="h-100">

        <div class="rightbar-title d-flex align-items-center px-3 py-4">

            <h5 class="m-0 me-2">Settings</h5>

            <a href="javascript:void(0);" class="right-bar-toggle ms-auto">
                <i class="mdi mdi-close noti-icon"></i>
            </a>
        </div>

        <!-- Settings -->
        <hr class="mt-0" />


        <div class="px-4 py-2">
            <h6 class="mb-3">Select Custome Colors</h6>
            <div class="form-check form-check-inline">
                <input class="form-check-input theme-color" type="radio" name="theme-mode"
                       id="theme-default" value="default" onchange="document.documentElement.setAttribute('data-theme-mode', 'default')" checked>
                <label class="form-check-label" for="theme-default">Default</label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input theme-color" type="radio" name="theme-mode"
                       id="theme-red" value="red" onchange="document.documentElement.setAttribute('data-theme-mode', 'red')">
                <label class="form-check-label" for="theme-red">Red</label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input theme-color" type="radio" name="theme-mode"
                       id="theme-teal" value="teal" onchange="document.documentElement.setAttribute('data-theme-mode', 'teal')">
                <label class="form-check-label" for="theme-teal">Teal</label>
            </div>
        </div>


        <h6 class="text-center mb-0 mt-3">Choose Layouts</h6>

        <div class="p-4">
            <div class="mb-2">
                <img src="<%= request.getContextPath()%>/resources/assets/images/layouts/layout-1.jpg" class="img-thumbnail" alt="">
            </div>
            <div class="form-check form-switch mb-3">
                <input type="checkbox" class="form-check-input theme-choice" id="light-mode-switch" checked />
                <label class="form-check-label" for="light-mode-switch">Light Mode</label>
            </div>

            <div class="mb-2">
                <img alt="" class="img-thumbnail" src="<%= request.getContextPath()%>/resources/assets/images/layouts/layout-2.jpg">
            </div>
            <div class="form-check form-switch mb-3">
                <input type="checkbox" class="form-check-input theme-choice" id="dark-mode-switch" data-bsStyle="<%= request.getContextPath()%>/resources/assets/css/bootstrap-dark.min.css" data-appStyle="<%= request.getContextPath()%>/resources/assets/css/app-dark.min.css" />
                <label class="form-check-label" for="dark-mode-switch">Dark Mode</label>
            </div>

            <div class="mb-2">
                <img alt="" class="img-thumbnail" src="<%= request.getContextPath()%>/resources/assets/images/layouts/layout-3.jpg">
            </div>
            <div class="form-check form-switch mb-5">
                <input type="checkbox" class="form-check-input theme-choice" id="rtl-mode-switch" data-appStyle="<%= request.getContextPath()%>/resources/assets/css/app-rtl.min.css" />
                <label class="form-check-label" for="rtl-mode-switch">RTL Mode</label>
            </div>


        </div>

    </div> <!-- end slimscroll-menu-->
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- JAVASCRIPT -->
<script src="<%= request.getContextPath()%>/resources/assets/libs/jquery/jquery.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/metismenu/metisMenu.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/simplebar/simplebar.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/node-waves/waves.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/jquery-sparkline/jquery.sparkline.min.js"></script>
<!--Morris Chart-->
<script src="<%= request.getContextPath()%>/resources/assets/libs/morris.js/morris.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/raphael/raphael.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/js/pages/dashboard.init.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/js/app.js"></script>
<!-- Plugins js -->
<script src="<%= request.getContextPath()%>/resources/assets/libs/dropzone/min/dropzone.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/js/pages/materialdesign.init.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/select2/js/select2.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/spectrum-colorpicker2/spectrum.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/admin-resources/bootstrap-filestyle/bootstrap-filestyle.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/js/pages/form-advanced.init.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/parsleyjs/parsley.min.js"></script>

<script src="<%= request.getContextPath()%>/resources/assets/js/pages/form-validation.init.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/js/pages/datatables.init.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<%= request.getContextPath()%>/resources/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyCtSAR45TFgZjOs4nBFFZnII-6mMHLfSYI"></script>


</body>


<!-- Mirrored from themesbrand.com/lexa/layouts/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 13 Jan 2023 07:48:46 GMT -->
</html>