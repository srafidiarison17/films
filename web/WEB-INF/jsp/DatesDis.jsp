
<%@page import="java.util.Arrays"%>
<%@page import="model.DispPl"%>
<%@page import="model.DispPl"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Princy
  Date: 28/01/2023
  Time: 12:41
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en" dir="ltr">


<!-- Mirrored from themesbrand.com/lexa/layouts/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 13 Jan 2023 07:47:17 GMT -->
<head>
    <meta charset="utf-8"/>
    <title>Calendrier de disponibilit&eacute;</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<%= request.getContextPath() %>/resources/assets/css/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">

</head>


<body data-sidebar="dark">

<!-- <body data-layout="horizontal" data-topbar="colored"> -->

<!-- Begin page -->
<div id="layout-wrapper">
    <div class="vertical-menu">
            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu list-unstyled" id="side-menu">
                    <li>
                        <a href="<%= request.getContextPath() %>/liste" class="waves-effect">
                            <span>Accueil</span>
                        </a>
                    </li>


                    <li>
                        <a href="<%= request.getContextPath() %>/ajoutFilm" class="waves-effect">
                            <span>Ajout Film</span>
                        </a>
                    </li>
                    <li>
                        <a href="<%= request.getContextPath() %>/listeFilm" class="waves-effect">
                            <span>Liste Film</span>
                        </a>
                    </li>

                    <li>
                        <a href="<%= request.getContextPath() %>/showPlanning" class="waves-effect">
                            <span>Planning</span>
                        </a>
                    </li>
                    
                </ul>
            </div>
            <!-- Sidebar -->
    </div>



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">


    <center><h2 style="color: #7b70be;"><i class="ion ion-ios-albums"></i> Dates Indispo</h2></center>


<br/>
<%
    String info = "";

    if(request.getAttribute("info")!=null){
        info = request.getAttribute("info").toString();%>

       <center>
           <div style="width: 400px;" class="alert alert-success" role="alert">
               <strong>Succes</strong> <%= info %>
           </div>
       </center>

    <%   } else if (request.getAttribute("error") !=null) {
        String error = request.getAttribute("error").toString();
    %>
        <center>
            <div style="width: 400px;" class="alert alert-danger mb-0" role="alert">
                <strong>Error</strong> <%= error %>
            </div>
        </center>
    <% } %>


    <br>

<div class="row" style="margin-left: 280px;">
    <div class="col-xl-6" style="width: 700px;">
        <div class="card">
            <div class="card-body">
               <center><h4 class="card-title">Ajouter une date d'indisponibilit&eacute;</h4></center>
                <form action="<%= request.getContextPath() %>/insertIndis" method="POST">
                    <div class="mb-3">
                        <label class="form-label">Plateau</label>
                        <select name="idPlateau">
                            <c:forEach var="plat" items="${allPlateau}">
                                <option value="${plat.idplateau}">${plat.nom}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Date</label>
                        <div>
                            <input name="dateinds" type="date" required placeholder="Date"/>
                        </div>
                    </div>
                    <div class="mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                                Add
                            </button>
                            <button type="reset" class="btn btn-secondary waves-effect">
                                Cancel
                            </button>
                        </div>
                    </div>

                </form>
            </div>
            <div>
                <center><h4 class="card-title">Toutes les dates d'indisponibilit&eacute;</h4></center>
                <table style="width: 500px">
                    <tr>
                        <td><b>Plateau</b></td>
                        <td><b>Date</b></td>
                    </tr>
                    <c:forEach var="d" items="${dp}">
                        <tr>
                            <td>${d.nom}</td>
                            <td>${d.dateind}</td>
                        </tr>
                    </c:forEach>
                </table>                        
            </div>
        </div>
    </div>
</div>
                    
    <div class="wrapper">
      <header>
        <p class="current-date"></p>
        <div class="icons">
          <span id="prev" class="material-symbols-rounded">chevron_left</span>
          <span id="next" class="material-symbols-rounded">chevron_right</span>
        </div>
      </header>
      <div class="calendar">
        <ul class="weeks">
          <li>Sun</li>
          <li>Mon</li>
          <li>Tue</li>
          <li>Wed</li>
          <li>Thu</li>
          <li>Fri</li>
          <li>Sat</li>
        </ul>
        <ul class="days"></ul>
      </div>
    </div>
                    
    <%
        ArrayList<DispPl> allIndispo = (ArrayList<DispPl>) request.getAttribute("dp");
        int[] days = new int[allIndispo.size()];
        int[] months = new int[allIndispo.size()];
        
        for(int i=0;i<allIndispo.size();i++){
            days[i] = allIndispo.get(i).getDateind().getDate();
            months[i] = allIndispo.get(i).getDateind().getMonth();
        }
        
        int arraySize = allIndispo.size();
    %>
  </body>
  
<script defer>
const daysTag = document.querySelector(".days"),
currentDate = document.querySelector(".current-date"),
prevNextIcon = document.querySelectorAll(".icons span");

// getting new date, current year and month
let date = new Date(),
currYear = date.getFullYear(),
currMonth = date.getMonth();

// storing full name of all months in array
const months = ["January", "February", "March", "April", "May", "June", "July",
              "August", "September", "October", "November", "December"];

const renderCalendar = () => {
    let firstDayofMonth = new Date(currYear, currMonth, 1).getDay(), // getting first day of month
    lastDateofMonth = new Date(currYear, currMonth + 1, 0).getDate(), // getting last date of month
    lastDayofMonth = new Date(currYear, currMonth, lastDateofMonth).getDay(), // getting last day of month
    lastDateofLastMonth = new Date(currYear, currMonth, 0).getDate(); // getting last date of previous month
    let liTag = "";
    var dayOff = <%= days[0]%>
    var arraySize = <%= arraySize %>

    for (let i = firstDayofMonth; i > 0; i--) { // creating li of previous month last days
        liTag += '<li class="inactive">'+(lastDateofLastMonth - i + 1)+'</li>';
    }

    for (let i = 1; i <= lastDateofMonth; i++) {
        // adding active class to li if the current day, month, and year matched
        let isToday = i === date.getDate() && currMonth === new Date().getMonth() 
                     && currYear === new Date().getFullYear() ? "active" : "";
        // Use a JSP expression to output the days and months arrays as JS arrays
        let days = [<%
            for (int i = 0; i < days.length; i++) {
                out.print(days[i]);
                if (i < days.length - 1) out.print(", "); // add comma if not last element
            }
        %>];
        let months = [<%
            for (int i = 0; i < months.length; i++) {
                out.print(months[i]);
                if (i < months.length - 1) out.print(", "); // add comma if not last element
            }
        %>];
        for(let j=0; j<days.length;j++){
            if(i===days[j]&&currMonth===months[j]){
                isToday = "inactive";
                break;
            }
        }
        liTag += '<li class="'+isToday+'">'+i+'</li>';
    }

    for (let i = lastDayofMonth; i < 6; i++) { // creating li of next month first days
        liTag += '<li class="inactive">'+(i - lastDayofMonth + 1)+'</li>';
    }
    currentDate.innerText = months[currMonth]+' '+currYear; // passing current mon and yr as currentDate text
    daysTag.innerHTML = liTag;
}
renderCalendar();

prevNextIcon.forEach(icon => { // getting prev and next icons
    icon.addEventListener("click", () => { // adding click event on both icons
        // if clicked icon is previous icon then decrement current month by 1 else increment it by 1
        currMonth = icon.id === "prev" ? currMonth - 1 : currMonth + 1;

        if(currMonth < 0 || currMonth > 11) { // if current month is less than 0 or greater than 11
            // creating a new date of current year & month and pass it as date value
            date = new Date(currYear, currMonth, new Date().getDate());
            currYear = date.getFullYear(); // updating current year with new date year
            currMonth = date.getMonth(); // updating current month with new date month
        } else {
            date = new Date(); // pass the current date as date value
        }
        renderCalendar(); // calling renderCalendar function
    });
});
    </script>
</html>