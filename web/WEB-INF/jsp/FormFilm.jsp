<%--
  Created by IntelliJ IDEA.
  User: Princy
  Date: 28/01/2023
  Time: 00:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"></jsp:include>


<center><h2 style="color: #7b70be;"><i class="ion ion-ios-albums"></i> Ajout Scène </h2></center>


<br/>
<%
  String info = "";

  if(request.getAttribute("info")!=null){
    info = request.getAttribute("info").toString();%>

<center>
  <div style="width: 400px;" class="alert alert-success" role="alert">
    <strong>Succes</strong> <%= info %>
  </div>
</center>

<%   } else if (request.getAttribute("error") !=null) {
  String error = request.getAttribute("error").toString();
%>
<center>
  <div style="width: 400px;" class="alert alert-danger mb-0" role="alert">
    <strong>Error</strong> <%= error %>
  </div>
</center>
<% } %>


<br>



<div class="row" style="margin-left: 280px;">
  <div class="col-xl-6" style="width: 700px;">
    <div class="card">
      <div class="card-body">
        <center><h4 class="card-title">Formulaire </h4></center>
        <form class="custom-validation" action="AjoutFilm" method="post" enctype="multipart/form-data">

          <div class="mb-3">
            <label class="form-label">Titre</label>
            <div>
              <input  name="titre" type="text"
                     class="form-control" required
                     placeholder="Titre"/>
            </div>
          </div>

          <div class="mb-3">
            <label>Dur&eacute;e</label>
            <div>
              <div class="input-group" >
                <input name="duree" type="time" class="form-control" >
              </div>
            </div>
          </div>


          <div class="mb-3">
            <label class="form-label">Auteur</label>
            <input type="text" name="auteur" class="form-control" >
          </div>

          <div class="mb-3">
            <label class="form-lable">Couverture</label>
            <input type="file" name="photo" class="form-control" data-buttonname="btn-secondary">
          </div>


          <div class="mb-0">
            <div>
              <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                Add
              </button>
              <button type="reset" class="btn btn-secondary waves-effect">
                Cancel
              </button>
            </div>
          </div>


        </form>
      </div>
    </div>
  </div>

</div>





<jsp:include page="footer.jsp"></jsp:include>