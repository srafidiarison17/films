drop database film;
create database film;
create role film login password 'film';
alter database film owner to film;
\c film film;

create table film(
	idFilm serial primary key,
	titre varchar(200), 
	duree time ,
	auteur varchar(100),
	couverture varchar(250)
);

insert into film (titre,duree,auteur,couverture) values ('Dora','02:15:00','Princy Rakotosalama','dora.jpg');
insert into film (titre,duree,auteur,couverture) values ('Le parfum vert','01:40:40','Fitia Faniry Andriamahefa','parfumvert.jpg');

create table acteur(
	idActeur serial primary key,
	nom varchar(100) 
);

insert into acteur (nom) values ('Alicia Rabiaza'),('Mandresy Rakoto'),('Elsy Charles'),('Murphy Raz'),('Jeanne Rousseau'),('Jacques Dutronc');


create table plateau(
	idPlateau serial primary key,
	nom varchar(100),
	longitude real not null,
 	latitude real not null
);

insert into plateau values(default,'Salon'),
('Cuisine'),
('Chambre a coucher'),
('Salle a manger'),
('Patisserie'),
('Boulangerie'),
('Casino'),
('Grande salle');


create table scene (
	idScene serial primary key,
	idPlateau int references plateau(idplateau),
	nom varchar(250), 
	duree time,
	idFilm int references film(idFilm)
);

insert into scene (idPlateau,nom,duree,idFilm) values (1,'Rencontre de Dora et Diego','00:01:00',1);
insert into scene (idPlateau,nom,duree,idFilm) values (3,'Preparation des affaires de Dora','00:02:00',1);
insert into scene (idPlateau,nom,duree,idFilm) values (1,'Au revoir avec les parents','00:03:00',1);
insert into scene (idPlateau,nom,duree,idFilm) values (5,'Achat de repas','00:03:00',1);
insert into scene (idPlateau,nom,duree,idFilm) values (9,'Petite priere avant depart','00:01:30',1);

insert into scene (idPlateau,nom,duree,idFilm) values (7,'Decouverte de l existence du parfum','00:04:00',2);
insert into scene (idPlateau,nom,duree,idFilm) values (8,'Conference sur le parfum','00:01:00',2);

create table action (
	idAction serial primary key,
	idScene int references scene(idScene),
	idActeur int references acteur(idActeur),
	sentiment varchar(50) ,
	dialogue varchar(250) ,
	action varchar(200) ,
	duree int ,
	ordre int
);

insert into action (idScene,idActeur,sentiment,dialogue,action,duree,ordre) values (1,1,'Chaleureuse','Hello Diego, je suis ta cousine Dora','Serre la main de Diego',3,1);
insert into action (idScene,idActeur,sentiment,dialogue,action,duree,ordre) values (1,2,'Chaleureux','Nice to meet you Dora','Serre la main de Dora',5,2);

insert into action (idScene,idActeur,sentiment,dialogue,action,duree,ordre) values (3,1,'Triste mais impatiente','','Calin avec les parents',3,1);
insert into action (idScene,idActeur,sentiment,dialogue,action,duree,ordre) values (3,3,'Triste','Prends soin de toi ma cherie','Calin avec Dora',4,2);
insert into action (idScene,idActeur,sentiment,dialogue,action,duree,ordre) values (3,5,'Triste','On t enverra de l argent de temps en temps','Calin avec Dora',4,3);

----------------------------------------------------------


create table dispoPlateau(
	idDispo serial primary key,
	idPlateau int,
	dateInd date
);

alter table scene add column etat int default 0;
alter table scene add horaireDebut time;
alter table scene add horaireFin time;

Create table tournage(
	idTournage serial primary key,
	idScene int,
	debut timestamp,
	fin timestamp
);


